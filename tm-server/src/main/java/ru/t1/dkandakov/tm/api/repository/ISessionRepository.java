package ru.t1.dkandakov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, created, user_id, role) " +
            "VALUES (#{id}, #{date}, #{userId}, #{role});")
    void add(@NotNull final SessionDTO session);

    @Delete("DELETE FROM tm_session WHERE id = #{id};")
    void removeById(@NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_session WHERE id = #{id};")
    void remove(@NotNull final SessionDTO session);

    @Nullable
    @Select("SELECT * FROM tm_session;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    List<SessionDTO> findAll();


    @Nullable
    @Select("SELECT * FROM tm_session WHERE id = #{id} and user_id = #{userId} LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    SessionDTO findOneById(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

}