package ru.t1.dkandakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.ProjectDTO;
import ru.t1.dkandakov.tm.enumerated.ProjectSort;
import ru.t1.dkandakov.tm.enumerated.Status;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @Nullable
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @Nullable
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId);

    @NotNull
    ProjectDTO removeOneById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    ProjectDTO findOneById(@Nullable String userId, @Nullable String id);

    void removeAll(@NotNull String userId);

    void removeAll();

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable ProjectSort sort);

    int getSize(@Nullable String userId);

    @Nullable
    ProjectDTO removeOneById(@Nullable String id);

    @Nullable
    ProjectDTO removeOneByIndex(@Nullable Integer index);

    @NotNull
    ProjectDTO removeOne(@Nullable ProjectDTO project);

    @Nullable
    ProjectDTO removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    ProjectDTO updateByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    );

    @Nullable
    ProjectDTO findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @Nullable
    ProjectDTO findOneById(@Nullable String id);

    @NotNull
    Collection<ProjectDTO> add(@NotNull Collection<ProjectDTO> models);

    @NotNull
    Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> models);

    @NotNull
    String getSortType(@Nullable ProjectSort sort);

    int getSize();

    @Nullable
    ProjectDTO changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}

