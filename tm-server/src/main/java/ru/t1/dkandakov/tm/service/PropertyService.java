package ru.t1.dkandakov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.api.service.IPropertyService;

import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "version";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "3333344";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "2222456";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String EMPTY_VALUE = "---";
    @NotNull
    public static final String SESSION_KEY = "session.key";
    @NotNull
    public static final String SESSION_KEY_DEFAULT = "qwertyu123";
    @NotNull
    public static final String SESSION_TIMEOUT = "session.timeout";
    @NotNull
    public static final String SESSION_TIMEOUT_DEFAULT = "10000";
    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";
    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";
    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String DATABASE_URL = "database.url";

    @NotNull
    private static final String DATABASE_USERNAME = "database.username";

    @NotNull
    private static final String DATABASE_USERNAME_DEFAULT = "postgres";

    @NotNull
    private static final String DATABASE_PASSWORD = "database.password";

    @NotNull
    private static final String DATABASE_PASSWORD_DEFAULT = "123";

    @NotNull
    private static final String DATABASE_DRIVER = "database.driver";

    @NotNull
    private static final String DATABASE_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private static final String DATABASE_DIALECT = "database.sql_dialect";

    @NotNull
    private static final String DATABASE_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_DEFAULT = "update";

    @NotNull
    private static final String DATABASE_SHOW_SQL = "database.show_sql";

    @NotNull
    private static final String DATABASE_SHOW_SQL_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_FORMAT_SQL = "database.format_sql";

    @NotNull
    private static final String DATABASE_FORMAT_SQL_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_COMMENT_SQL = "database.comment_sql";

    @NotNull
    private static final String DATABASE_COMMENT_SQL_DEFAULT = "true";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String value = getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
        return Integer.parseInt(value);
    }

    @Override
    @NotNull
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT, SESSION_TIMEOUT_DEFAULT);
    }


    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().contains(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseURL() {
        return getStringValue(DATABASE_URL);
    }

    @NotNull
    @Override
    public String getDatabaseUsername() {
        return getStringValue(DATABASE_USERNAME, DATABASE_USERNAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD, DATABASE_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER, DATABASE_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBDialect() {
        return getStringValue(DATABASE_DIALECT, DATABASE_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO, DATABASE_HBM2DDL_AUTO_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBShowSql() {
        return getStringValue(DATABASE_SHOW_SQL, DATABASE_SHOW_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBFormatSql() {
        return getStringValue(DATABASE_FORMAT_SQL, DATABASE_FORMAT_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBCommentsSql() {
        return getStringValue(DATABASE_COMMENT_SQL, DATABASE_COMMENT_SQL_DEFAULT);
    }

}