package ru.t1.dkandakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.TaskDTO;
import ru.t1.dkandakov.tm.enumerated.Status;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @NotNull
    TaskDTO create(@Nullable TaskDTO model);

    @NotNull
    TaskDTO create(@Nullable String userId, String name);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @NotNull String description);

    @NotNull
    Collection<TaskDTO> add(@NotNull Collection<TaskDTO> models);

    @NotNull
    Collection<TaskDTO> set(@NotNull Collection<TaskDTO> models);

    @Nullable
    List<TaskDTO> findAll();

    @Nullable
    List<TaskDTO> findAll(@Nullable Comparator<TaskDTO> comparator);

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator<TaskDTO> comparator);

    @Nullable
    TaskDTO findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAll();

    @NotNull
    TaskDTO removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable TaskDTO removeOneById(@Nullable String id);

    @NotNull TaskDTO removeOne(@Nullable TaskDTO task);

    void removeAll(@Nullable String userId);

    @NotNull
    TaskDTO update(@NotNull TaskDTO model);

    @NotNull
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    TaskDTO removeOneByIndex(@Nullable Integer index);

    @Nullable
    TaskDTO removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    int getSize();

    @NotNull
    TaskDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    TaskDTO findOneByIndex(@Nullable Integer index);

    @Nullable
    TaskDTO findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @Nullable
    TaskDTO findOneById(@Nullable String id);

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId);

    @NotNull
    TaskDTO changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    int getSize(@Nullable String userId);
}