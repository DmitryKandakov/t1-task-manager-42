--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: tm_project; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tm_project (
    id text,
    name text,
    description text,
    user_id text,
    status text,
    created text
);


ALTER TABLE public.tm_project OWNER TO postgres;

--
-- Name: tm_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tm_session (
    id text,
    created text,
    user_id text,
    role text
);


ALTER TABLE public.tm_session OWNER TO postgres;

--
-- Name: tm_task; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tm_task (
    id text,
    name text,
    description text,
    user_id text,
    status text,
    created text,
    project_id text
);


ALTER TABLE public.tm_task OWNER TO postgres;

--
-- Name: tm_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tm_user (
    login text,
    id text,
    password_hash text,
    fst_name text,
    lst_name text,
    mdl_name text,
    email text,
    role text,
    lock_flg text DEFAULT false
);


ALTER TABLE public.tm_user OWNER TO postgres;

--
-- Data for Name: tm_project; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tm_project (id, name, description, user_id, status, created) FROM stdin;
b8d1437b-392f-499d-941b-c76a7a48e2cd	retrety	reyreye	1	NOT_STARTED	2022-07-18 19:23:33.861+05
6fc88b3c-b1ff-45fb-816a-6db3561bfaae	dertewt	345346	1	NOT_STARTED	2022-07-19 14:25:10.274+05
f5d684dc-4e64-492b-a3a8-5d531409c0ff	ertrety	reyrey	f2f75f4c-3b2f-4db2-ac10-622f15f462ee	NOT_STARTED	2022-07-19 14:36:00.146+05
3eb1ac06-7497-48fb-a098-f35e12baed74	retrey	retyrey	f2f75f4c-3b2f-4db2-ac10-622f15f462ee	NOT_STARTED	2022-07-19 14:36:21.279+05
4a019b77-7e42-4bf2-8b78-21561d321831	4365478	6666666666	1	NOT_STARTED	2022-07-21 12:04:15.123+05
\.


--
-- Data for Name: tm_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tm_session (id, created, user_id, role) FROM stdin;
eea7e351-7822-40d0-b331-15e9f4226d24	2022-07-18 19:01:38.456+05	1	USUAL
52f5448f-8df9-4c8c-b87b-c1c89d64e2a4	2022-07-18 19:23:09.497+05	1	USUAL
08748fba-de15-4043-a923-0364ff109a00	2022-07-19 14:08:57.876+05	1	USUAL
8e1a53c2-e339-44d7-b55e-b1d6a0c4bbf1	2022-07-19 14:11:22.443+05	1	USUAL
9fd9c6ac-bd1b-4aee-8988-ab88f814eee3	2022-07-19 14:13:49.95+05	1	USUAL
fc9f2540-655a-46a8-a804-4585235287ac	2022-07-19 14:24:56.514+05	1	USUAL
1b29ecfb-f788-40ca-afaa-10961185cdf1	2022-07-19 14:35:39.112+05	f2f75f4c-3b2f-4db2-ac10-622f15f462ee	USUAL
f5c319f2-8b0d-4be8-a82c-f5d31bf60d4f	2022-07-19 14:40:10.161+05	1	USUAL
27cf2cd6-ccef-40b5-827c-770c552ffd3a	2022-07-19 14:40:41.774+05	1cc7ed7e-0f00-46ec-b220-2bec9684f155	USUAL
a223c31d-f02c-4cb8-a24b-dccad39bfeae	2022-07-19 14:46:37.076+05	1cc7ed7e-0f00-46ec-b220-2bec9684f155	USUAL
d422526b-5e82-4d22-a7fb-8ecf60bb53db	2022-07-19 15:01:24.833+05	1cc7ed7e-0f00-46ec-b220-2bec9684f155	USUAL
9a90f14b-ea69-43de-a6a7-82820b501e37	2022-07-19 15:21:29.391+05	1	USUAL
166d27be-8253-494b-adce-0a592affbac1	2022-07-19 15:22:07.204+05	1	USUAL
4224934c-d798-40a2-9c00-cf05167ede75	2022-07-19 15:23:09.407+05	6aa62fed-3b2e-4751-a818-1860818f3220	USUAL
607b6e25-68bb-42cd-88ed-8c9fffe04dae	2022-07-19 15:26:23.133+05	6aa62fed-3b2e-4751-a818-1860818f3220	USUAL
f8a5fe0d-4726-4da1-9a8b-165e320b73ea	2022-07-19 15:53:11.245+05	1	ADMIN
34195c43-8bb8-4398-b6aa-6c2f59f376ee	2022-07-19 15:54:54.672+05	1	ADMIN
cbf6cff8-2cc3-406a-923a-cd0ce5608f9d	2022-07-19 17:41:57.069+05	6aa62fed-3b2e-4751-a818-1860818f3220	ADMIN
cfbc213a-3ddf-4fe1-8088-556e5321531f	2022-07-19 18:02:11.954+05	6aa62fed-3b2e-4751-a818-1860818f3220	ADMIN
5c9de87a-9234-4d6f-931c-c2a72e56140f	2022-07-21 11:50:35.996+05	1	ADMIN
819a37df-73db-4a43-92da-e91290641197	2022-07-21 12:03:32.051+05	1	ADMIN
5fa99f3f-dbe2-4537-9432-d1fe429c0576	2022-07-21 12:04:31.056+05	6aa62fed-3b2e-4751-a818-1860818f3220	ADMIN
4c9cb5c1-bc70-4aec-85d8-ac7e776b8d75	2022-07-21 12:13:28.37+05	6aa62fed-3b2e-4751-a818-1860818f3220	ADMIN
9679621e-04dd-4644-80ff-2d43032b8410	2022-07-21 12:27:08.544+05	6aa62fed-3b2e-4751-a818-1860818f3220	ADMIN
c9df044d-e1f0-4bb9-9f61-26070942e8dc	2022-07-21 12:29:17.073+05	6aa62fed-3b2e-4751-a818-1860818f3220	ADMIN
6e432bd5-631e-46a1-b1b6-e2f5ec2a1987	2022-07-21 12:32:59.279+05	6aa62fed-3b2e-4751-a818-1860818f3220	ADMIN
476cba41-2769-4d40-9df7-5e8dead989f9	2022-07-21 12:38:57.877+05	6aa62fed-3b2e-4751-a818-1860818f3220	ADMIN
fcf15ce8-9bf1-4668-83c0-f037262f94e8	2022-07-21 13:02:50.882+05	1	ADMIN
a3bb5430-66f5-435a-a273-3e28e0452c4a	2022-07-21 13:12:35.172+05	1	ADMIN
dfa4fd47-ccae-4991-bc4a-e348ee80d7ae	2022-07-21 13:23:24.673+05	1	ADMIN
5e4f07a3-ec09-4f29-9645-da4be5f5efbe	2022-07-21 13:29:40.053+05	1	ADMIN
57ed830a-e725-418f-95e0-bb5abfac320d	2022-07-21 13:56:33.331+05	1	ADMIN
cff4d301-2e79-4f98-94c4-c92e30df5631	2022-07-21 13:56:46.345+05	1	ADMIN
eccb42d2-d6b2-47f2-823f-d410f9ab72b2	2022-07-21 13:57:52.155+05	1	ADMIN
a1a83ccc-4641-46dc-bf8b-a5859cd91f80	2022-07-21 13:59:42.611+05	1	ADMIN
3af7e0ad-9d8a-4cb2-9c33-667198210a8d	2022-07-21 14:10:44.054+05	1	ADMIN
957b6639-ee61-4d46-93b2-958b36a07be1	2022-07-21 14:17:47.318+05	1	ADMIN
9c6cb834-35d1-40cc-9914-1ed75048795a	2022-07-21 14:24:34.919+05	1	ADMIN
ed44fcc5-5096-4258-9b01-ea2c4f403012	2022-07-21 14:34:39.327+05	1	ADMIN
bfa25726-ea78-4207-ad3b-6ca101b6949d	2022-07-21 14:45:44.263+05	1	ADMIN
4bd575d9-a675-4a98-9935-57176bfbd081	2022-07-21 14:54:45.765+05	1	ADMIN
f54b0868-1ef6-4ec0-8a87-0e2b9f6d6c4b	2022-07-21 14:59:15.713+05	1	ADMIN
\.


--
-- Data for Name: tm_task; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tm_task (id, name, description, user_id, status, created, project_id) FROM stdin;
3389f2e9-c58e-4a77-b844-660c67193174	3456346	ewtrewt	1	NOT_STARTED	2022-07-18 19:24:07.272+05	\N
262ea98e-e73e-4cda-9f01-c9a020328f13	3333	333	6aa62fed-3b2e-4751-a818-1860818f3220	NOT_STARTED	2022-07-19 17:42:37.212+05	\N
bc0ecb43-c9b5-4d4d-91da-e4430e585ede	3453456	436346	6aa62fed-3b2e-4751-a818-1860818f3220	NOT_STARTED	2022-07-21 12:33:55.258+05	\N
\.


--
-- Data for Name: tm_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tm_user (login, id, password_hash, fst_name, lst_name, mdl_name, email, role, lock_flg) FROM stdin;
admin	1	a42c306e7c87534901d85e9195318817	\N	\N	\N	\N	ADMIN	\N
123	f2f75f4c-3b2f-4db2-ac10-622f15f462ee	a42c306e7c87534901d85e9195318817	\N	\N	\N	12@11.ae	ADMIN	\N
222	6aa62fed-3b2e-4751-a818-1860818f3220	f573bc7c14550ca99fc5e1e7497cd5fe	\N	\N	\N	2222	ADMIN	\N
etret	f6bf3010-0e95-41be-aa1e-a43986ee3d36	a28301fd62c85ba1df597cbf67a93971	\N	\N	\N	reytrey	USUAL	\N
ewrtew	54a79852-901f-4e2e-9783-b0484cae37ac	ff85131bd51be556ecb9b413868c4bf6	\N	\N	\N	ewtew	USUAL	false
\.


--
-- PostgreSQL database dump complete
--

