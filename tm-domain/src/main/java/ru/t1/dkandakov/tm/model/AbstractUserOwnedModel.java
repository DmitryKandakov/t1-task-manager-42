package ru.t1.dkandakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Setter
@Getter
@NoArgsConstructor
@MappedSuperclass
public class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    @ManyToOne
    private User user;

}