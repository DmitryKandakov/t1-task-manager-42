package ru.t1.dkandakov.tm.api.endpoint;

import ru.t1.dkandakov.tm.dto.request.AbstractRequest;
import ru.t1.dkandakov.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface IOperation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}