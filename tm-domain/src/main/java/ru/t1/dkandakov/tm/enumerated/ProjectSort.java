package ru.t1.dkandakov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.comparator.CreatedComparator;
import ru.t1.dkandakov.tm.comparator.NameComparator;
import ru.t1.dkandakov.tm.comparator.StatusComparator;
import ru.t1.dkandakov.tm.dto.model.ProjectDTO;

import java.util.Comparator;

@Getter
public enum ProjectSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_DEFAULT("Without sort", null);

    @Getter
    @NotNull
    private final String name;

    @Getter
    @Nullable
    private final Comparator<ProjectDTO> comparator;

    ProjectSort(@NotNull final String displayName, @Nullable final Comparator<ProjectDTO> comparator) {
        this.name = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public static ProjectSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return BY_DEFAULT;
        for (@NotNull final ProjectSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return BY_DEFAULT;
    }

}