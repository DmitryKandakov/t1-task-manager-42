package ru.t1.dkandakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkandakov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dkandakov.tm.api.endpoint.IUserEndpoint;
import ru.t1.dkandakov.tm.dto.model.UserDTO;
import ru.t1.dkandakov.tm.dto.request.user.UserLoginRequest;
import ru.t1.dkandakov.tm.dto.request.user.UserRegistryRequest;
import ru.t1.dkandakov.tm.dto.request.user.UserRemoveRequest;
import ru.t1.dkandakov.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.dkandakov.tm.dto.response.user.UserLoginResponse;
import ru.t1.dkandakov.tm.dto.response.user.UserRegistryResponse;
import ru.t1.dkandakov.tm.dto.response.user.UserRemoveResponse;
import ru.t1.dkandakov.tm.dto.response.user.UserUpdateProfileResponse;
import ru.t1.dkandakov.tm.marker.ISoapCategory;

import java.util.UUID;

@Category(ISoapCategory.class)
public final class UserEndpointTest {

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @Before
    public void init() {
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        adminToken = adminResponse.getToken();
        @NotNull final UserLoginResponse userResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        userToken = userResponse.getToken();
    }

    @Test
    public void registryUser() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest()
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest(null, "login", "password", "email")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest("", "login", "password", "email")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest(userToken, null, "password", "email")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest(userToken, "login", null, "email")
        ));
        @Nullable final UserRegistryResponse response = userEndpoint.registryUser(
                new UserRegistryRequest(
                        adminToken,
                        "testUser" + UUID.randomUUID().toString(),
                        "password",
                        UUID.randomUUID().toString()
                )
        );
        Assert.assertNotNull(response.getUser());
        @Nullable final UserDTO user = response.getUser();
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals(response.getUser().getLogin(), user.getLogin());
        @Nullable final UserLoginResponse login = authEndpoint.login(
                new UserLoginRequest(response.getUser().getLogin(), "password")
        );
        Assert.assertTrue(login.getSuccess());
    }

    @Test
    public void removeUser() {
        UserRegistryResponse userRegistryResponse = userEndpoint.registryUser(new UserRegistryRequest(
                adminToken,
                "testUser" + UUID.randomUUID().toString(),
                "password",
                UUID.randomUUID().toString())
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest()
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest(null, "test")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest("", "test")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest(userToken, null)
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest(userToken, UUID.randomUUID().toString())
        ));
        @Nullable final UserRemoveResponse response = userEndpoint.removeUser(
                new UserRemoveRequest(adminToken, userRegistryResponse.getUser().getLogin())
        );
        Assert.assertNotNull(response.getUser());
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(
                new UserLoginRequest(adminToken, userRegistryResponse.getUser().getLogin())
        ));
    }

    @Test
    public void updateUserProfile() {
        @NotNull final String firstName = "firstName";
        @NotNull final String lastName = "lastName";
        @NotNull final String middleName = "middleName";
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest()
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest(null, firstName, lastName, middleName)
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest("", firstName, lastName, middleName)
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest("token", firstName, lastName, middleName)
        ));
        @Nullable final UserUpdateProfileResponse response = userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest(userToken, firstName, lastName, middleName)
        );
        Assert.assertNotNull(response.getUser());
        @Nullable final UserDTO user = response.getUser();
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals(lastName, user.getLastName());
        userEndpoint.updateUserProfile(new UserUpdateProfileRequest(
                userToken, firstName + "UPD", lastName + "UPD", middleName + "UPD"
        ));
    }

}
