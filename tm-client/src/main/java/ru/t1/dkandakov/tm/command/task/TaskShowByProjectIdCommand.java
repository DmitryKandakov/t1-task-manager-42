package ru.t1.dkandakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.task.TaskListByProjectIdRequest;
import ru.t1.dkandakov.tm.dto.response.task.TaskListByProjectIdResponse;
import ru.t1.dkandakov.tm.util.TerminalUtil;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID: ");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskListByProjectIdRequest request =
                new TaskListByProjectIdRequest(getToken(), projectId);
        @NotNull final TaskListByProjectIdResponse response = getTaskEndpoint().listTaskByProjectId(request);
        renderTasks(response.getTasks());
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task list by project id";
    }

}