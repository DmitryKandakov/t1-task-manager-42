package ru.t1.dkandakov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.dkandakov.tm.api.endpoint.*;
import ru.t1.dkandakov.tm.api.repository.ICommandRepository;
import ru.t1.dkandakov.tm.api.service.*;
import ru.t1.dkandakov.tm.command.AbstractCommand;
import ru.t1.dkandakov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.dkandakov.tm.exception.system.CommandNotSupportedException;
import ru.t1.dkandakov.tm.repository.CommandRepository;
import ru.t1.dkandakov.tm.service.CommandService;
import ru.t1.dkandakov.tm.service.LoggerService;
import ru.t1.dkandakov.tm.service.PropertyService;
import ru.t1.dkandakov.tm.service.TokenService;
import ru.t1.dkandakov.tm.util.SystemUtil;
import ru.t1.dkandakov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.dkandakov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);


    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ITokenService tokenService = new TokenService();

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    public void run(@NotNull String[] args) {
        prepareStartup();
        processArguments(args);
        processCommands();
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom((clazz))) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND: ");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void processArguments(final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            @NotNull final String argument = args[0];
            processArgument(argument);
            loggerService.command(argument);
            System.exit(0);
        } catch (@NotNull final Exception e) {
            System.err.println("[FAIL]");
            loggerService.error(e);
            System.exit(1);
        }
    }

    public void processArgument(@NotNull final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void processCommand(@NotNull final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void processCommand(@NotNull final String command) {
        processCommand(command, true);
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

}