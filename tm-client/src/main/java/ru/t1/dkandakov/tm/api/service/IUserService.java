package ru.t1.dkandakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.UserDTO;
import ru.t1.dkandakov.tm.enumerated.Role;

public interface IUserService extends IService<UserDTO> {

    @NotNull
    UserDTO create(String login, String password);

    @NotNull
    UserDTO create(String login, String password, String email);

    @NotNull
    UserDTO create(String login, String password, Role role);

    @Nullable
    UserDTO findByLogin(String login);

    @Nullable
    UserDTO findByEmail(String email);

    @Nullable
    UserDTO removeByLogin(String login);

    @Nullable
    UserDTO removeByEmail(String email);

    @NotNull
    UserDTO setPassword(String id, String password);

    @NotNull
    UserDTO updateUser(
            String id,
            String firstName,
            String lastName,
            String middleName
    );

    @NotNull
    UserDTO lockUserByLogin(String login);

    @NotNull
    UserDTO unlockUserByLogin(String login);

    @NotNull
    Boolean isLoginExist(String login);

    @Nullable
    Boolean isEmailExist(String email);

}